import React, { Component } from 'react';

export default class Form1 extends React.Component{
    constructor(props){
        super(props)
        this.state ={
            input :  "",
            submit : ""
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }
    handleChange(event){
        this.setState({
            input: event.target.value
        });

    }
    handleSubmit(event){
        event.preventDefault()
        this.setState({
            submit: this.state.input
        });

    }
   /* <h1>Hello {this.state.name}</h1>
    <button onClick={this.change_name}>Change Name</button>*/
    render(){
    return(
        <div>
   
     <br>
     </br>
     <form onSubmit={this.handleSubmit}>
        <input value={this.state.input} onChange={this.handleChange}></input>
        <button type="submit">Input</button>
     </form>
        <h1>{this.state.submit}</h1>
        <h1>{this.state.input}</h1>
        </div>
    
    );
    }
}