import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Comp from './pages/page1/Comp.jsx';

import {Link, Switch} from 'react-router-dom';
import {
  Route,
  BrowserRouter,
  Router
} from 'react-router-dom';

import reportWebVitals from './reportWebVitals';
import Myfirstcomp from './pages/page2/FirstComp.jsx';

ReactDOM.render(
  
  <BrowserRouter key="router">
  <Switch>
    <Route exact path='/' component={Comp}/>
    <Route exact path='/page1' component={Myfirstcomp}/>
  </Switch>
</BrowserRouter>,
//console.log("index.js Rendered");
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
