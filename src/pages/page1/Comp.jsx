import logo from '../../logo.svg';
import '../../App.css';
import '../page1/Comp.css'
import Myfirstcomp from '../page2/FirstComp';
import '../../index.css';

import {Link, Switch} from 'react-router-dom';
import {
  Route,
  BrowserRouter,
  Router
} from 'react-router-dom';

function Comp() {
  return (
    <div className="App">
      
      <header className="App-header">
      <Myfirstcomp name="Roma"></Myfirstcomp>
      
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    
    </div>
    
  );
}



export default Comp;
