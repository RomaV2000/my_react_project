
import React, { Component } from 'react';
import Form1 from '../../components/form1/Form1';
import Form2 from '../../components/form2/Form2';
import Menu from '../../components/menu/Menu.jsx';
import * as remx from 'remx';

export default class Myfirstcomp extends React.Component{
    
    
    constructor(props){
        super(props)
        this.state = {
            //name : "Alex"
           count: 0
        }
       // this.change_name = this.change_name.bind(this);
        this.add_count = this.add_count.bind(this);
        this.dec_count = this.dec_count.bind(this);
        this.zero_count = this.zero_count.bind(this);
    }
    add_count(){
        this.setState( state =>( {
           // name: "Vova"
          count: state.count + 1
        }));
        
    }
    dec_count(){
        this.setState( state =>( {
           // name: "Vova"
          count: state.count - 1
        }));
    }
    zero_count(){
        this.setState( state =>( {
           // name: "Vova"
          count: 0
        }));
    }
   /* <h1>Hello {this.state.name}</h1>
    <button onClick={this.change_name}>Change Name</button>*/
    render(){
        console.log("FirsComp.jsx Rendered");
    return(
        <div>
        <Menu></Menu>
        <h1>Hello {this.props.name}</h1>
 
        <h1>{this.state.count}</h1>
      
        <button onClick={this.add_count}>Plus</button>
        <button onClick={this.dec_count}>Minus</button>
        <button onClick={this.zero_count}>Zero</button>
        <Form1></Form1>
        <Form2></Form2>
        </div>
    
    );
    
    }
}
//export default Myfirstcomp;
Myfirstcomp.defaultProps = {name:"Dima"};